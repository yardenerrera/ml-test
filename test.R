#yarden Ererra 307915900
setwd("H:/MLTest")

#Q1.1
students.raw <- read.csv('dataBase.csv')
students.prepared<-students.raw
str(students.prepared)
summary(students.prepared)
dim(students.prepared)

#Q1.2
any(is.na(students.prepared)) #False

#Q1.3
#install.packages("ggplot2")
library(ggplot2)
ggplot(students.prepared, aes(higher,G3)) + geom_boxplot() #the attribute "highr" has effect on the grade, if the student wants to take higher education, the grade will be higher

ggplot(students.prepared, aes(G3)) + geom_bar()

#Q2.1
ggplot(students.prepared, aes(school,G3)) + geom_boxplot()#yes, students from GP will have a highr grade

#Q2.2
hist(students.prepared$absences, 100)#histogram with exceptions
hist(students.prepared$absences, 100, xlim= c(0,25))#histogram without exceptions

#Q2.3
#new parameter
breaks <- c(0,12,20)
labels <- c("unsuccess","success")

#bin the data 
bins <- cut(students.prepared$G3, breaks = breaks, include.lowest = T,right = F, labels = labels)
summary(bins)
students.prepared$isSuccess <- bins
str(students.prepared)
#histogram
ggplot(students.prepared, aes(absences, fill=isSuccess)) + geom_histogram(binwidth=5)
ggplot(students.prepared, aes(absences, fill=isSuccess)) + geom_histogram(position = 'fill')# there is affect , more absenccess, less success

#Q2.4
ggplot(students.prepared, aes(failures, fill=isSuccess)) + geom_bar()
ggplot(students.prepared, aes(failures, fill=isSuccess)) + geom_bar(position = 'fill')#more past class failures, less success

#Q3.1
#remove unrellevant attributes
students.prepared$id<-NULL

#separate into training set ans set
library(caTools)

filter1 <- sample.split(students.prepared$G3, SplitRatio = 0.7)

students.train <- subset(students.prepared, filter1==T)
students.test <- subset(students.prepared, filter1==F)

dim(students.test)
dim(students.train)
dim(students.prepared)

#Q3.2

#run algorithm to get model 
str(students.train)

model <- lm(G3 ~ ., students.train)

summary(model)

predicted.train <- predict(model,students.train)
predicted.test <- predict(model,students.test)


MSE.train <- mean((students.train$G3 - predicted.train)**2)#7.929422
MSE.test <- mean((students.test$G3 - predicted.test)**2)#10.02835

RMSE.train <- MSE.train**0.5 #2.815923
RMSE.test <- MSE.test**0.5 #3.166757
RMSE.test
#not overfotting and not under fitting. the RMSE not to close and not to far

#Q3.3
#new parameter
breaks1 <- c(0,11,20)
labels1 <- c("notAccepted","accepted")

#bin the data 
bins1 <- cut(students.prepared$G3, breaks = breaks1, include.lowest = T,right = F, labels = labels1)
summary(bins1)
students.prepared$isAccepted <- bins1

summary(students.prepared$isAccepted)

#new terms(include the grade 11):
Table1<-table(students.prepared$isAccepted)

PerOfFailures<- (Table1[1]/(Table1[1]+Table1[2]))*100# percent of failures:  47.08861% 
PerOfFailures

model <- lm(G3 ~ ., students.train)

summary(model)

########################################################

#old terms (not include the grade 11):
Table2<-table(students.prepared$isSuccess)

PerOfFailures2<-(Table2[1]/(Table2[1]+Table2[2]))*100# percent of failures:  58.98734% 

DecPer<-PerOfFailures2-PerOfFailures
DecPer # new terms decreased failures in  11.89873%


#Q4.1
students.prepared$G3<- as.factor(students.prepared$G3)#G3 as Factor
summary(students.prepared$G3)

x<-(395*0.2)
x
Y<-1+5+12+6+16+33#minimum grade to Excellence- 15
Y

#new parameter- excellence
breaks2 <- c(0,15,20)
labels2 <- c("FALSE","TRUE")
students.prepared$G3<- as.integer(students.prepared$G3)#G3 as integer
#bin the data 
bins2 <- cut(students.prepared$G3, breaks = breaks2, include.lowest = T,right = F, labels = labels2)
summary(bins2)
students.prepared$excellence <- bins2
str(students.prepared)
summary(students.prepared$excellence)

#Q4.2

filter2 <- sample.split(students.prepared$traveltime, SplitRatio = 0.7)

students.train2 <- subset(students.prepared, filter2==T)
students.test2 <- subset(students.prepared, filter2==F)

dim(students.test2)
dim(students.train2)
dim(students.prepared)

#BN Model
#install.packages('e1071')
library(e1071)

modelBN <- naiveBayes(students.train2,students.train2$excellence)

#prediction on the test set 
prediction <- predict(modelBN,students.test2, type = 'raw')

prediction_excellence <- prediction[,'TRUE']

predicted <- prediction_excellence > 0.5
actual <- students.test2$excellence
#confusion matrix 
conf_matrix <- table(predicted,actual)
conf_matrix

#compute precision and recall 
precision1 <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[2,1])
recall1 <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[1,2])
precision1
recall1
summary(modelBN)

#Q4.3
#Tree Model
#install.packages('rpart')
library(rpart)
#install.packages('rpart.plot')
library(rpart.plot)
modelT <- rpart(excellence~.,students.train2)

rpart.plot(modelT,box.palette = "RdBu", shadow.col = "grey", nn=TRUE)

predict.prob <- predict(modelT, students.test2)
predict.prob

prediction_excellence2 <- predict.prob[,'TRUE']
prediction2 <- prediction_excellence2 > 0.5
prediction2

actual2 <- students.test2$excellence

cf <- table(prediction2,actual2)
cf

precision <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['TRUE','FALSE'])
recall <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['FALSE','TRUE'])

precision
recall

#Q4.4
#ROC curve
#install.packages('pROC')
library(pROC)

rocBN <- roc(students.test2$excellence,prediction_excellence, direction = ">", levels = c('TRUE','FALSE'))
rocT <- roc(students.test2$excellence,prediction_excellence2, direction = ">", levels = c('TRUE','FALSE'))

plot(rocBN, col= 'red', main = 'ROC chart')
par(new=TRUE) #see the 2 line in one screen
plot(rocT, col= 'blue', main = 'ROC chart')

auc(rocBN) #area under curve
auc(rocT)

